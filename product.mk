TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := arm

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/toolchains/arm-linux-musleabihf/bin/arm-linux-musleabihf-

TARGET_GLOBAL_CFLAGS += -march=armv7-a

HOST_CPP = cc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

TARGET_SKEL_DIRS := $(TOP_DIR)/trail/arm-generic/config/
